import React,{PureComponent} from 'react';
import { PersistGate } from 'redux-persist/es/integration/react'
import { Provider } from 'react-redux';
import AppNavigator from "./Screeens/Navigation/AppNavigator";
import { store, persistor } from './Store/store';
import firebase from 'react-native-firebase';
import AsyncStorage from '@react-native-community/async-storage'
import GoogleSigin from './Screeens/GoogleSigin';
import StudentDetails from './Screeens/StudentDetails';


export default class App extends PureComponent{
  constructor(props) {
    super()
    this.state = {
      id:''
    }
  }
   // firebase permission function
 checkPermission = async () => {       
  const enabled = await firebase.messaging().hasPermission();
  if (enabled) {
      this.getFcmToken();
  } else {
      this.requestPermission();
  }
}

// firebase  getting token
getFcmToken = async () => {
  const fcmToken = await firebase.messaging().getToken();
  if (fcmToken) {
    console.log(fcmToken);
    const token = AsyncStorage.getItem('token')
      if (token!== null) {
          AsyncStorage.setItem('token',fcmToken);
    // this.showAlert('Your Firebase Token is:', fcmToken);
        }
  } else {
    this.showAlert('Failed', 'No token received');
  }
}

//firebase request persmission function
requestPermission = async () => {
  try {
    await firebase.messaging().requestPermission();
    // User has authorised
  } catch (error) {
      // User has rejected permissions
  }
}

//firebase message Listener
messageListener = async () => {

  this.notificationListener = firebase.notifications().onNotification((notification) => {
      const { title, body } = notification;
      this.showAlert(title, body);

      //  console.log(title,body)

      
  });

  this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
    const { title, body } = notificationOpen.notification;
      this.showAlert(title, body);
      // console.log(title,body)
    
      //  this.showAlert(title, body);

  });

  const notificationOpen = await firebase.notifications().getInitialNotification( );
  if (notificationOpen) {
    console.log(notificationOpen,"data")
    console.log(title,body)

      const { title, body } = notificationOpen.notification;
      const notification =  AsyncStorage.getItem('userNotification')
      if (notification!== null) {
        AsyncStorage.setItem('userNotification',"true");
  }
}

  this.messageListener = firebase.messaging().onMessage((message) => {
    console.log(JSON.stringify(message));
  });
}

showAlert = (title, message) => {
  Alert.alert(
    title,
    message,
    [
      {text: 'OK', onPress: () => console.log('OK Pressed')},
    ],
    {cancelable: false},
  );
}
 

componentDidMount=async()=>{  
 const result=await AsyncStorage.getItem('id')
 this.setState({id:result})
 console.log(this.state.id)
 this.checkPermission();        // firebase permission
 this.messageListener(); 
}

  render (){
return (
// Redux: Global Store
<Provider store={store}>
<PersistGate 
        loading={null}
        persistor={persistor}
      >
     {this.state.id==null ?
    <GoogleSigin/>:<AppNavigator/>}
{/* <AppNavigator/> */}
</PersistGate>
 </Provider>
);
}
}