import React, { Component } from 'react';
import { View, Text, Button, Platform, TouchableOpacity,Animated,StyleSheet } from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
const moment = require('moment');
import PhoneInput from "react-native-phone-number-input";
import { Appbar } from 'react-native-paper';

class Picker extends Component {
  constructor(props) {
    super(props);
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    this.phoneInput = React.createRef();
    this.state = {
      value: '',
      formattedValue: '',
      valid: false,
      showMessage: false,
      date: new Date(("1990-01-01")),
      mode: 'date',
      show: false,
      pickerData: '',
      fadeValue: new Animated.Value(0)

    };
  }
  start = () => {
    
      const checkValid = this.phoneInput.current?.isValidNumber(this.state.value);
      this.setState({ showMessage: true, valid: checkValid ? checkValid : false })
  
    Animated.timing(this.state.fadeValue, {
      toValue: 1,
      duration: 1000
    }).start();
  };

  handleBackButtonClick() {
    this.props.navigation.goBack(null);
    return true;
  }

  onChange = (event, selectedDate) => {
    const currentDate = selectedDate || this.state.date;
    this.setState({ show: Platform.OS === 'ios' })
    this.setState({ date: currentDate });
  };

  showMode = (currentMode) => {
    this.setState({ mode: currentMode, show: !this.state.show });
  };

  showDatepicker = () => {
    this.showMode('date');
  };

  showTimepicker = () => {
    this.showMode('time');
  };


  render() {
    return (
      <View style={{ flex: 1, }}>

        <Appbar.Header
          style={{ backgroundColor: 'white', }}>
          <Appbar.BackAction
            onPress={this.handleBackButtonClick} />
          <Appbar.Content
            title="Retreving Data"
            titleStyle={{
              fontSize: 16, fontWeight: "bold",
              color: 'steelblue'
            }} />
        </Appbar.Header>

        <View style={{ justifyContent: 'center', alignSelf: 'center', marginTop: 20 }}>
          <Button
            onPress={() => this.showDatepicker()}
            title="Show date picker!" />
          <Text
            style={{ padding: 10, backgroundColor: 'grey', marginTop: 10 }}>
            {moment(this.state.date).format('DD-MMM-YYYY')}
          </Text>
        </View>

        <View style={{ marginTop: 10, justifyContent: 'center', alignSelf: 'center' }}>
          <Button
            onPress={() => this.showTimepicker()}
            title="Show time picker!" />
          <Text
            style={{ padding: 10, backgroundColor: 'grey', marginTop: 10 }}>
            {moment(this.state.date).format('h:mm a')}
          </Text>
        </View>

        {this.state.show && (
          <DateTimePicker
            testID="dateTimePicker"
            value={this.state.date}
            mode={this.state.mode}
            is24Hour={true}
            display="default"
            onChange={this.onChange}
          />
        )}
        <View style={{ justifyContent: 'center', alignSelf: 'center', marginTop: 20 }}>
          <PhoneInput
            ref={this.phoneInput}
            defaultValue={this.state.value}
            defaultCode="DM"
            onChangeText={(text) => {
              this.setState({ value: text });
            }}
            onChangeFormattedText={(text) => {
              this.setState({ formattedValue: text });
            }}
            withDarkTheme
            withShadow
            autoFocus={false}
          />
          <View style={{marginTop:20,alignSelf:'center'}}>

           <TouchableOpacity style={styles.btn} onPress={() => this.start()}>
          <Text style={styles.textBtn}> Check</Text>
        </TouchableOpacity>
        {this.state.showMessage && (

        <Animated.View
          style={{
            opacity: this.state.fadeValue,
            height: 150,
            width: 200,
            margin: 5,
            borderRadius: 12,
            marginLeft:5,
            backgroundColor: "#347a2a",
            justifyContent: "center",
          
          }} >
               <Text>Value : {this.state.value}</Text>
              <Text>Formatted Value : {this.state.formattedValue}</Text>
              <Text>Valid : {this.state.valid ? "true" : "false"}</Text>     
                 </Animated.View>
               )}
          </View>
         
        </View>
      </View>
    );
  }
}

export default Picker;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFF",
    alignItems: "center",
    justifyContent: "center"
  },
  item: {},
  btn: {
    backgroundColor: "#480032",
    width: 100,
    height: 40,
    padding: 3,
    justifyContent: "center",
    alignSelf:'center',
    borderRadius: 6
  },
  text: {
    fontSize: 20,
    color: "#fff",
    fontWeight: "bold",
    textAlign: "center"
  },
  item1: {
    backgroundColor: "red",
    padding: 20,
    width: 100,
    margin: 10
  },

  textBtn: {
    color: "#f4f4f4",
    fontWeight: "bold",
    textAlign: "center"
  }
});