import React from "react";
import { View, Text, StyleSheet, SafeAreaView, TextInput, TouchableHighlight, ScrollView, Alert, Image, TouchableOpacity, Dimensions } from "react-native";
import { connect } from "react-redux"
import { Appbar, Caption } from 'react-native-paper';
import { saveStudentDetails } from "../Action/saveStudentDetails";
import BottomSheet from 'reanimated-bottom-sheet';
import Animated from 'react-native-reanimated';
import DocumentPicker from 'react-native-document-picker';
import ImagePicker from 'react-native-image-crop-picker';
import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes,
} from '@react-native-community/google-signin';
import AsyncStorage from '@react-native-community/async-storage'


class StudentDetails extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      name: "",
      status: '',
      Email: '',
      image: 'https://api.adorable.io/avatars/80/abott@adorable.png',
      schoolName: "",
      tenMark: '',
      twlveMark: '',
      collegeName: "",
      cgpa: "",
      fileName: 'Choose file',
      resume: [],
      extn: '',
      userInfo: {},
      ref: '',
    }
  }

  submit = () => {
    if (this.state.name.length == 0 || this.state.Email.length == 0 || this.state.schoolName.length == 0 || this.state.tenMark.length == 0 || this.state.twlveMark.length == 0 || this.state.collegeName.length == 0 || this.state.cgpa.length == 0) {
      Alert.alert('forms')
    }
    else {
      var details = {};
      details.name = this.state.name;
      details.Email = this.state.Email;
      details.schoolName = this.state.schoolName;
      details.tenMark = this.state.tenMark;
      details.twlveMark = this.state.twlveMark;
      details.collegeName = this.state.collegeName;
      details.cgpa = this.state.cgpa;
      details.fileName = this.state.fileName;
      details.image = this.state.image
      this.props.reduxSaveStudentDetail(details)
      this.props.navigation.navigate("ShowStudentDetail")
    }

  }
  takePhotoFromCamera = () => {
    ImagePicker.openCamera({
      compressImageMaxWidth: 300,
      compressImageMaxHeight: 300,
      cropping: true,
      compressImageQuality: 0.7
    }).then(image => {
      console.log(image);
      this.setState({ image: image.path });
      this.bs.current.snapTo(1);
    });
  }

  choosePhotoFromLibrary = () => {
    ImagePicker.openPicker({
      width: 300,
      height: 300,
      cropping: true,
      compressImageQuality: 0.7,
    }).then(image => {
      console.log(image);
      this.setState({ image: image.path });
      this.bs.current.snapTo(1);
    });
  }

  renderInner = () => (
    <View style={styles.panel}>
      <View style={{ alignSelf: 'center' }}>
        <Text style={styles.panelTitle}>Upload Photo</Text>
        <Text style={styles.panelSubtitle}>Choose Your Profile Picture</Text>
      </View>
      <TouchableOpacity style={styles.panelButton} onPress={this.takePhotoFromCamera}>
        <Text style={styles.panelButtonTitle}>Take Photo</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.panelButton} onPress={this.choosePhotoFromLibrary}>
        <Text style={styles.panelButtonTitle}>Choose From Library</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.panelButton}
        onPress={() => this.bs.current.snapTo(1)}>
        <Text style={styles.panelButtonTitle}>Cancel</Text>
      </TouchableOpacity>
    </View>
  );

  renderHeader = () => (
    <View style={styles.header}>
      <View style={styles.panelHeader}>
        <View style={styles.panelHandle} />
      </View>
    </View>
  );

  bs = React.createRef();
  fall = new Animated.Value(1);

  selectMultipleFile = async () => {
    try {
      const results = await DocumentPicker.pickMultiple({
        type: [DocumentPicker.types.allFiles],
      });
      this.setState({ resume: results, fileName: results[0].name });

    } catch (err) {
      //Handling any exception (If any)
      if (DocumentPicker.isCancel(err)) {
        //If user canceled the document selection
        alert('Canceled from multiple doc picker');
      } else {
        //For Unknown Error
        alert('Unknown Error: ' + JSON.stringify(err));
        throw err;
      }
    }
  };
  toggleStatus = () => {
    this.setState({ status: !this.state.status })
  }
  signOut = async () => {
    console.log('inn')
    const ref = await AsyncStorage.getItem('ref')
    this.setState({ ref: ref })
    console.log(this.state.ref)
    try {
      if (this.state.ref === 'true') {
        await GoogleSignin.configure({});
        await GoogleSignin.revokeAccess();
        await GoogleSignin.signOut();
        await AsyncStorage.removeItem('id').then(
          this.props.navigation.navigate('GoogleSigin'),
        );
      }
      if (this.state.ref === 'false') {
        await AsyncStorage.removeItem('id').then(
          this.props.navigation.navigate('GoogleSigin'),
        );
      }
    } catch (error) {
      console.error(error);
    }
  };

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <Appbar.Header style={{ backgroundColor: 'white' }}>
          <Appbar.Action icon="menu" color={"steelblue"} size={30}
            onPress={() => { this.toggleStatus() }} />
          <Appbar.Content title="Task"
            titleStyle={{
              fontSize: 22, fontWeight: "bold",
              marginLeft: 30,
              color: 'steelblue'
            }} />
        </Appbar.Header>
        <View>
          {this.state.status == true ?
            <View style={styles.burgermenu}>
              <View style={{ padding: 10 }}>

                <Caption style={styles.text}
                  onPress={() => this.props.navigation.navigate('ApiIntegration')}>
                  Apis Integration
              </Caption>
                <Caption
                  style={styles.text}
                  onPress={() => this.props.navigation.navigate('Video')}>
                  Video
              </Caption>
                <Caption
                  style={styles.text}
                  onPress={() => this.props.navigation.navigate('Picker')}>
                  Date and Time Picker
              </Caption>
              </View>
              <Text
                style={{ padding: 30, fontSize: 15, fontWeight: 'bold' }}
                onPress={() => this.signOut()}>
                Log out
              </Text>
            </View> : null
          }
        </View>

        <BottomSheet
          ref={this.bs}
          snapPoints={[450, 0]}
          renderContent={this.renderInner}
          renderHeader={this.renderHeader}
          initialSnap={1}
          callbackNode={this.fall}
          enabledGestureInteraction={true}
        />

        <Animated.View style={{
          margin: 20,
          opacity: Animated.add(0.1, Animated.multiply(this.fall, 1.0)),
        }}>
          <View style={{ alignItems: 'center' }}>
            <Image
              source={{
                uri: this.state.image,
              }}
              style={{ height: 100, width: 100, borderRadius: 15, backgroundColor: 'white' }}
              imageStyle={{ borderRadius: 15 }}>
            </Image>
            <TouchableOpacity style={{ marginTop: 20 }} onPress={() => this.bs.current.snapTo(0)}>
              <Text style={{ fontSize: 20 }}>Upload Photo</Text>
            </TouchableOpacity>
          </View>
        </Animated.View>

        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.mainView}>
            <Text style={styles.mainTextStyle}>Form</Text>
            <Text style={styles.textStyle}>Enter Your Name</Text>
            <TextInput
              style={styles.textInputStyle}
              underlineColorAndroid="transparent"
              placeholderTextColor="#cccccc"
              placeholder="Enter Your Name"
              onChangeText={name => {
                this.setState({ name: name }, () => {
                });
              }}
            />
            <Text style={styles.textStyle}>Enter Your Email</Text>
            <TextInput
              style={styles.textInputStyle}
              underlineColorAndroid="transparent"
              placeholderTextColor="#cccccc"
              placeholder="Enter Your Email "
              keyboardType='email-address'
              onChangeText={Email => {
                this.setState({ Email: Email }, () => {
                });
              }}
            />
            <Text style={styles.textStyle}>Enter Your schoolName</Text>
            <TextInput
              style={styles.textInputStyle}
              underlineColorAndroid="transparent"
              placeholderTextColor="#cccccc"
              placeholder="Enter Your School Name"
              onChangeText={schoolName => {
                this.setState({ schoolName: schoolName }, () => {
                });
              }}
            />
            <Text style={styles.textStyle}>Enter Your Tenth Mark</Text>
            <TextInput
              style={styles.textInputStyle}
              underlineColorAndroid="transparent"
              placeholderTextColor="#cccccc"
              placeholder="Enter Your  Tenth Mark"
              onChangeText={tenMark => {
                this.setState({ tenMark: tenMark }, () => {
                });
              }}
            />
            <Text style={styles.textStyle}>Enter Your Twelveth Mark</Text>
            <TextInput
              style={styles.textInputStyle}
              underlineColorAndroid="transparent"
              placeholderTextColor="#cccccc"
              placeholder="Enter Your Twelveth Mark"
              onChangeText={twlveMark => {
                this.setState({ twlveMark: twlveMark }, () => {
                });
              }}
            />
            <Text style={styles.textStyle}>Enter Your CollegeName</Text>
            <TextInput
              style={styles.textInputStyle}
              underlineColorAndroid="transparent"
              placeholderTextColor="#cccccc"
              placeholder="Enter Your College Name"
              onChangeText={collegeName => {
                this.setState({ collegeName: collegeName }, () => {
                });
              }}
            />
            <Text style={styles.textStyle}>Enter Your CGPA</Text>
            <TextInput
              style={styles.textInputStyle}
              underlineColorAndroid="transparent"
              placeholderTextColor="#cccccc"
              placeholder="Enter Your CGPA"
              onChangeText={cgpa => {
                this.setState({ cgpa: cgpa }, () => {
                });
              }}
            />
            <Text style={styles.textStyle}>Resume</Text>
            <Text style={{ padding: 10, backgroundColor: 'white', width: '100%', marginTop: 10 }}
              onPress={() => this.selectMultipleFile()}>
              {this.state.fileName}{this.state.extn}
            </Text>

            <TouchableHighlight
              underlayColor="transparent"
              style={
                styles.buttonStyle
              }
              onPress={() => {
                this.submit()
              }}>
              <Text style={styles.buttonTextStyle}>Submit</Text>
            </TouchableHighlight>
          </View>

        </ScrollView>
      </SafeAreaView>
    )
  }
}
const { width } = Dimensions.get("screen");
const width_space = width / 3;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "steelblue",
  },
  bottom: {
    position: 'absolute',
    backgroundColor: 'white',
    left: 0,
    right: 0,
    bottom: 0,
    top: 0
  },
  text: { fontWeight: "bold", fontSize: 14, color: "black", padding: 5 },
  burgermenu: { height: 900, backgroundColor: 'white', width: 230, position: 'absolute', zIndex: 1, alignItems: 'flex-start' },

  bayRow: {
    width: width_space,
    marginBottom: 30,
  },
  slots: {
    justifyContent: "center",
    alignItems: "center",
    padding: 20,
    backgroundColor: 'grey'
  },

  mainTextStyle: {
    width: "100%",
    height: 40,
    textAlign: "center",
    marginTop: 10,
    marginBottom: 10,
    fontSize: 20
  },
  mainView: {
    width: "100%",
    height: "50%",
    justifyContent: "flex-start",
    alignItems: "center",
    padding: 10,
  },
  textInputStyle: {
    width: "100%",
    height: 40,
    backgroundColor: "white",
    paddingHorizontal: 15,
    marginBottom: 10,
    marginTop: 10
  },
  textStyle: {
    width: "100%",
    height: 20,
    //paddingHorizontal:15,
    textAlign: "left",
    marginTop: 10,
    fontSize: 15
  },
  mainTextStyle: {
    width: "100%",
    height: 40,
    //paddingHorizontal:15,
    textAlign: "center",
    marginTop: 10,
    marginBottom: 10,
    fontSize: 20
  },
  buttonStyle: {
    width: "100%",
    height: 40,
    borderRadius: 5,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "white",
    marginTop: 20
  },
  buttonTextStyle: {
    width: "100%",
    height: "100%",
    textAlign: "center",
    marginTop: 10,
    fontSize: 18
  },
  panel: {
    padding: 20,
    backgroundColor: '#FFFFFF',
    height: 500
  },
  header: {
    backgroundColor: '#FFFFFF',
    shadowColor: '#333333',
    shadowOffset: { width: -1, height: -3 },
    shadowRadius: 2,
    shadowOpacity: 0.4,
    // elevation: 5,
    paddingTop: 20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  panelHeader: {
    alignItems: 'center',
  },
  panelTitle: {
    alignSelf: 'center',
    height: 35,
  },
  panelHandle: {
    width: 40,
    height: 8,
    borderRadius: 4,
    backgroundColor: '#00000040',
    marginBottom: 10,
  },
  panelSubtitle: {
    fontSize: 14,
    color: 'gray',
    height: 30,
    marginBottom: 10,
  },
  panelButton: {
    padding: 13,
    borderRadius: 10,
    backgroundColor: 'steelblue',
    alignItems: 'center',
    marginVertical: 7,
  },
  panelButtonTitle: {
    fontSize: 17,
    fontWeight: 'bold',
    color: 'white',
  },
})

const mapDispatchToProps = (dispatch) => {
  return {
    reduxSaveStudentDetail: (Details) => dispatch(saveStudentDetails(Details))
  }
}
export default connect(
  null,
  mapDispatchToProps
)(StudentDetails);