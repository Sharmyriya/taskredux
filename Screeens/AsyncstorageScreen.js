import React, { Component } from 'react'
import { View, Text,Animated,TouchableOpacity } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage'
import { Appbar } from 'react-native-paper';

class AsyncStorageScreen extends Component {
    static navigationOptions = {
        header: null,
    }

    constructor() {
        super()
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
        this.state = {
            name: '',
            email: '',
            schoolName: '',
            collegeName: ''
        }
    }

    componentDidMount = async () => {
        const name = await AsyncStorage.getItem('userName')
        const email = await AsyncStorage.getItem('userEmail')
        const schoolName = await AsyncStorage.getItem('userSchool')
        const collegeName = await AsyncStorage.getItem('userCollege')
        this.setState({ name: name, email: email, schoolName: schoolName, collegeName: collegeName })
        console.log(name, email, 'storing data')
    }

    handleBackButtonClick() {
        this.props.navigation.goBack(null);
        return true;
    }
  
    render() {

        return (
            <View style={{ flex: 1, backgroundColor: 'steelblue' }}>
                <Appbar.Header 
                style={{ backgroundColor: 'white', }}>
                    <Appbar.BackAction 
                    onPress={this.handleBackButtonClick} />
                    <Appbar.Content 
                    title="Retreving Data"
                    titleStyle={{
                            fontSize: 16, fontWeight: "bold",
                            color: 'steelblue'
                        }} />
                </Appbar.Header>

                <View style={container}>
                    <View style={{ marginTop: 5, flexDirection: 'row' }}>
                        <Text style={{ fontSize: 15, fontWeight: 'bold', width: '30%' }}>Name</Text>
                        <Text style={{ fontSize: 15 }}>:  {this.state.name}</Text>
                    </View>

                    <View style={{ marginTop: 5, flexDirection: 'row' }}>
                        <Text style={{ fontSize: 15, fontWeight: 'bold', width: '30%',lineHeight:20 }}>Email</Text>
                        <Text style={{ fontSize: 15,lineHeight:20 }}>: {this.state.email}</Text>
                    </View>

                    <View style={{ marginTop: 5, flexDirection: 'row' }}>
                        <Text style={{ fontSize: 15, fontWeight: 'bold', width: '35%',lineHeight:20 }}>School Name</Text>
                        <Text style={{ fontSize: 15,lineHeight:20 }}>: {this.state.schoolName}</Text>
                    </View>

                    <View style={{ marginTop: 5, flexDirection: 'row' }}>
                        <Text style={{ fontSize: 15, fontWeight: 'bold', width: '35%',lineHeight:20 }}>college Name</Text>
                        <Text style={{ fontSize: 15,lineHeight:20 }}>:  {this.state.collegeName}</Text>
                    </View>
                    
                </View>
            </View>
        )
    }
}


const container = {
    flex: 1,
    backgroundColor: 'steelblue',
    marginTop: 30,
    padding:20
}

 const box= {
    backgroundColor: 'blue',
    width: 50,
    height: 100
 }

export default AsyncStorageScreen;