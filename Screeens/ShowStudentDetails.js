import React from "react";
import { View, Text, SafeAreaView, StyleSheet, Image, Dimensions } from "react-native";
import { connect } from "react-redux";
import { Appbar, Caption } from 'react-native-paper'
import AsyncStorage from '@react-native-community/async-storage'
import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes,
} from '@react-native-community/google-signin';

class ShowStudentDetail extends React.Component {
  constructor(props) {
    super()
    this.state = {
      status: false,
      ref: '',
    }
  }
  toggleStatus = () => {
    this.setState({ status: !this.state.status })
  }
  aysncFunction = async () => {
    await AsyncStorage.setItem('userName', this.props.details.name)
    await AsyncStorage.setItem('userEmail', this.props.details.Email)
    await AsyncStorage.setItem('userSchool', this.props.details.schoolName)
    await AsyncStorage.setItem('userCollege', this.props.details.collegeName)
    this.props.navigation.navigate('AsyncstorageScreen')
  }



  render() {
    return (
      <View style={styles.container1}>
        <Appbar.Header style={{ backgroundColor: 'white', }}>
          <Appbar.Action icon="menu" color={"steelblue"} size={30}
            onPress={() => { this.toggleStatus() }} />
          <Appbar.Content title="Detail"
            titleStyle={{
              fontSize: 20, fontWeight: "bold",
              color: 'steelblue'
            }} />
        </Appbar.Header>
        <View>
          {this.state.status == true ?
            <View style={styles.burgermenu}>

              <View style={{ padding: 10 }}>
                <Text style={styles.text}
                  onPress={() => this.aysncFunction()}>
                  AsyncStorage
              </Text>
              </View>
            </View> : null
          }
        </View>


        <SafeAreaView style={styles.container}>
          <View style={[styles.textViewStyle, { alignSelf: 'center', marginTop: 20 }]}>

            <Image
              source={{
                uri: this.props.details.image,
              }}
              style={{ height: 100, width: 100, borderRadius: 15, backgroundColor: 'white' }}
              imageStyle={{ borderRadius: 15 }}>

            </Image>
          </View>
          <View style={styles.textViewStyle}>
            <Text style={styles.textStyle}>Name   </Text>
            <Text style={[styles.textStyle, { fontSize: 13 }]}>: {this.props.details.name}</Text>
          </View>
          <View style={styles.textViewStyle}>
            <Text style={styles.textStyle}>Email    </Text>
            <Text style={[styles.textStyle, { fontSize: 13 }]}>: {this.props.details.Email}</Text>
          </View>
          <View style={styles.textViewStyle}>
            <Text style={styles.textStyle}>School Name    </Text>
            <Text style={[styles.textStyle, { fontSize: 13 }]}>: {this.props.details.schoolName}</Text>
          </View>
          <View style={styles.textViewStyle}>
            <Text style={styles.textStyle}>Tenth Mark    </Text>
            <Text style={[styles.textStyle, { fontSize: 13 }]}>: {this.props.details.tenMark}</Text>
          </View>
          <View style={styles.textViewStyle}>
            <Text style={styles.textStyle}>Twelveth Mark    </Text>
            <Text style={[styles.textStyle, { fontSize: 13 }]}>: {this.props.details.twlveMark}</Text>
          </View>
          <View style={styles.textViewStyle}>
            <Text style={styles.textStyle}>College Name   </Text>
            <Text style={[styles.textStyle, { fontSize: 13 }]}>: {this.props.details.collegeName}</Text>
          </View>
          <View style={styles.textViewStyle}>
            <Text style={styles.textStyle}>CGPA   </Text>
            <Text style={[styles.textStyle, { fontSize: 13 }]}>: {this.props.details.cgpa}</Text>
          </View>
          <View style={styles.textViewStyle}>
            <Text style={styles.textStyle}>Uploaded Resume   </Text>
            <Text style={[styles.textStyle, { fontSize: 13 }]}>: {this.props.details.fileName}</Text>
          </View>
        </SafeAreaView>
      </View>
    )
  }
}
const { width } = Dimensions.get("screen");
const width_space = width / 3;

const styles = StyleSheet.create({
  container1: {
    backgroundColor: 'white',
    height: Dimensions.get('window').height,
    width: Dimensions.get('window').width,
    flex: 1
  },

  container: {
    flex: 1,

    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    backgroundColor: "steelblue",
  },
  textViewStyle: {
    flexDirection: "row",
    paddingBottom: 20,
    marginHorizontal: 20
  },
  textStyle: {
    width: '50%',
    // height:0,
    textAlign: "left",
    lineHeight: 20,
    fontSize: 15
  },
  text: { fontWeight: "bold", fontSize: 14, color: "black", padding: 5 },
  burgermenu: { height: 900, backgroundColor: 'white', width: 230, position: 'absolute', zIndex: 1, alignItems: 'flex-start' },

  bayRow: {
    width: width_space,
    marginBottom: 30,
  },
  slots: {
    justifyContent: "center",
    alignItems: "center",
    padding: 20,
    backgroundColor: 'grey'
  },

  mainTextStyle: {
    width: "100%",
    height: 40,
    textAlign: "center",
    marginTop: 10,
    marginBottom: 10,
    fontSize: 20
  },
})
const mapStateToProps = (state) => {
  return {
    details: state.StudentDetailReducer.details
  }
}
export default connect(mapStateToProps, null)(ShowStudentDetail)