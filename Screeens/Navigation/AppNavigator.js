import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import StudentDetails from '../StudentDetails';
import ShowStudentDetail from '../ShowStudentDetails';
import AsyncstorageScreen from '../AsyncstorageScreen';
import ApiIntegration from '../ApiIntegration';
import Picker from '../Picker';
import GoogleSigin from '../GoogleSigin';
import Video from '../Video'
const RootStack = createStackNavigator();

const AppNavigator = ({navigation}) => (
    <NavigationContainer>
    <RootStack.Navigator headerMode='none' >
    <RootStack.Screen name="GoogleSigin" component={GoogleSigin}/>
        <RootStack.Screen name="StudentDetails" component={StudentDetails}/>
        <RootStack.Screen name="ShowStudentDetail" component={ShowStudentDetail}/>
            <RootStack.Screen name="AsyncstorageScreen" component={AsyncstorageScreen}/>
            <RootStack.Screen name="ApiIntegration" component={ApiIntegration}/>
            <RootStack.Screen name="Picker" component={Picker}/>
            <RootStack.Screen name="Video" component={Video}/>
    </RootStack.Navigator>
    </NavigationContainer>
);

export default AppNavigator;
