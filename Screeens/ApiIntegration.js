import React, { Component } from 'react';
import { View, Text, FlatList } from 'react-native';
import axios from 'react-native-axios';
import { Appbar } from 'react-native-paper';

class ApiIntegration extends Component {
    constructor(props) {
        super(props);
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
        this.state = {
            listData: []
        };
    }

    componentDidMount = () => {
        this.getuserId()
    }

    getuserId = () => {
        axios.get('https://jsonplaceholder.typicode.com/todos')
            .then(response => {
                this.setState({ listData: response.data })
            },
            )
            .catch(function (error) {
                console.log(error);
            });
    }

    renderCard = ({ item, index }) => {
        return (
            <View
                style={{
                    padding: 10,
                    backgroundColor: "#F4F6F6",
                    marginHorizontal: 10, marginTop: 10,
                    borderRadius: 10,
                    elevation: 15,
                    height: 80
                }} >
                <View style={{ marginTop: 5 }}>
                    <Text> UserId: {item.userId}</Text>
                    <Text> Title: {item.title}</Text>
                </View>
            </View>
        )
    }
    handleBackButtonClick() {
        this.props.navigation.goBack(null);
        return true;
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'steelblue' }}>
                <Appbar.Header
                    style={{ backgroundColor: 'white', }}>
                    <Appbar.BackAction
                        onPress={this.handleBackButtonClick} />
                    <Appbar.Content
                        title="List"
                        titleStyle={{
                            fontSize: 16, fontWeight: "bold",
                            color: 'steelblue'
                        }} />
                </Appbar.Header>

                <FlatList
                    data={this.state.listData}
                    renderItem={({ item, index }) => this.renderCard({ item, index })}
                    keyExtractor={(item, index) => item + index}
                />
            </View>
        );
    }
}

export default ApiIntegration;
