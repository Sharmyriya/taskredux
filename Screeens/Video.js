import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import VideoPlayer from 'react-native-video-controls';
import ImagePicker from 'react-native-image-crop-picker';
import { Appbar } from 'react-native-paper';


class Video extends Component {
  constructor(props) {
    super(props);
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    this.state = {
      image: 'https://vjs.zencdn.net/v/oceans.mp4',
    };
  }

  takeVideoFromCamera = () => {
    ImagePicker.openCamera({
      mediaType: 'video',
    }).then(image => {
      console.log(image);
      this.setState({ image: image.path });
    });
  }

  chooseVideoFromLibrary = () => {
    ImagePicker.openPicker({
      mediaType: 'video',
      width: 300,
      height: 300,
    }).then(image => {
      console.log(image);
      this.setState({ image: image.path });
    });
  }

  handleBackButtonClick() {
    this.props.navigation.goBack(null);
    return true;
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <Appbar.Header
          style={{ backgroundColor: 'white', }}>
          <Appbar.BackAction
            onPress={this.handleBackButtonClick} />
          <Appbar.Content
            title="Video"
            titleStyle={{
              fontSize: 16, fontWeight: "bold",
              color: 'steelblue'
            }} />
        </Appbar.Header>

        <TouchableOpacity style={styles.panelButton}
          onPress={this.takeVideoFromCamera}>
          <Text
            style={styles.panelButtonTitle}>
            Take Video
            </Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.panelButton}
          onPress={this.chooseVideoFromLibrary}>
          <Text
            style={styles.panelButtonTitle}>
            Choose From Library
            </Text>
        </TouchableOpacity>

        <View style={{ backgroundColor: 'green', height: 400 }}>
          <VideoPlayer
            source={{ uri: this.state.image }}
            navigator={this.props.navigator}
            tapAnywhereToPause={true}
            paused={true}
          />
        </View>
      </View>
    );
  }
}

export default Video;
const styles = StyleSheet.create({

  panel: {
    padding: 20,
    backgroundColor: '#FFFFFF',
    height: 500
  },

  panelTitle: {
    alignSelf: 'center',
    height: 35,
  },
  panelHandle: {
    width: 40,
    height: 8,
    borderRadius: 4,
    backgroundColor: '#00000040',
    marginBottom: 10,
  },
  panelSubtitle: {
    fontSize: 14,
    color: 'gray',
    height: 30,
    marginBottom: 10,
  },
  panelButton: {
    padding: 13,
    borderRadius: 10,
    backgroundColor: 'steelblue',
    alignItems: 'center',
    marginVertical: 7,
  },
  panelButtonTitle: {
    fontSize: 17,
    fontWeight: 'bold',
    color: 'white',
  },
})
