import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes,
} from '@react-native-community/google-signin';
import AsyncStorage from "@react-native-community/async-storage";
import {
  LoginButton,
  AccessToken,
  GraphRequest,
  GraphRequestManager,
} from 'react-native-fbsdk';

class GoogleSigin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userInfo: '',
      userName: '',
      token: '',
      profilePic: ''
    };
  }

  componentDidMount = () => {
    GoogleSignin.configure({
      webClientId: '470494495172-inb0h13qghvidstmm6hi4eelt5the6eo.apps.googleusercontent.com', // client ID of type WEB for your server(needed to verify user ID and offline access)
      offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
      forceCodeForRefreshToken: true, // [Android] related to `serverAuthCode`, read the docs link below *.
      accountName: '', // [Android] specifies an account name on the device that should be used
    });
  }

  getInfoFromToken = async (token) => {
    const PROFILE_REQUEST_PARAMS = {
      fields: {
        string: 'id, name,  first_name, last_name',
      },
    };
    const profileRequest = new GraphRequest(
      '/me',
      { token, parameters: PROFILE_REQUEST_PARAMS },
      async (error, result) => {
        if (error) {
          console.log('login info has error: ' + error);
        } else {
          console.log('result:', result);
          this.setState({ userInfo: result.name });
          const results = await AsyncStorage.setItem('id', this.state.userInfo)
          await AsyncStorage.setItem('ref', 'false')
          if (results == null) {
            this.props.navigation.navigate('StudentDetails')
            console.log('result:', result);
          } else {
            this.props.navigation.navigate('StudentDetails')

          }
        }
      },
    );
    new GraphRequestManager().addRequest(profileRequest).start();
  };
  signIn = async () => {
    try {
      await GoogleSignin.hasPlayServices();
      const info = await GoogleSignin.signIn();
      console.warn({ userInfo: info.user.email });
      this.setState({ userInfo: info.user.email })
      // await AsyncStorage.setItem('id', this.state.userInfo)
      const result = await AsyncStorage.setItem('id', this.state.userInfo)
      await AsyncStorage.setItem('ref', 'true')
      if (result !== null) {
        this.props.navigation.navigate('StudentDetails')
        console.log('result:', result);
      } else {
        this.props.navigation.navigate('StudentDetails')
      }
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation (e.g. sign in) is in progress already
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // play services not available or outdated
      } else {
        // some other error happened
      }
    }
  };

  render() {

    return (
      <View style={styles.container}>

        {/* Google Login */}
        <GoogleSigninButton
          style={{ width: 192, height: 78 }}
          size={GoogleSigninButton.Size.Wide}
          color={GoogleSigninButton.Color.Dark}
          onPress={this.signIn}
        />

        {/* FaceBook Login */}
        <View style={{marginTop:20}}>
        <LoginButton
        
          onLoginFinished={(error, result) => {
            if (error) {
              console.log('login has error: ' + result.error);
            } else if (result.isCancelled) {
              console.log('login is cancelled.');
            } else {
              AccessToken.getCurrentAccessToken().then(data => {
                const accessToken = data.accessToken.toString();
                this.getInfoFromToken(accessToken);
              });
            }
          }}
        />
        </View>
      </View>
    );
  }
}

export default GoogleSigin;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: "steelblue",
    justifyContent: 'center',
    paddingHorizontal: 10,
  },

})