import { combineReducers } from 'redux';

// Imports: Reducers

import StudentDetailReducer from "./StudentDetailReducer"

// Redux: Root Reducer
const rootReducer = combineReducers({
  
    StudentDetailReducer:StudentDetailReducer
});

// Exports
export default rootReducer;